var indexSectionsWithContent =
{
  0: "abcdefghiklmnprstx",
  1: "abegilmps",
  2: "gx",
  3: "abcdegiklms",
  4: "a",
  5: "abcfghilpst",
  6: "r",
  7: "ln"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties",
  6: "events",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Properties",
  6: "Events",
  7: "Pages"
};


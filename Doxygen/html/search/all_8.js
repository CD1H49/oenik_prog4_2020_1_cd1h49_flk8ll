var searchData=
[
  ['igamegraphicsrenderer_52',['IGameGraphicsRenderer',['../interface_game___wpf_1_1_i_game_graphics_renderer.html',1,'Game_Wpf.IGameGraphicsRenderer'],['../interface_game_1_1_graphics_renderer_1_1_i_game_graphics_renderer.html',1,'Game.GraphicsRenderer.IGameGraphicsRenderer']]],
  ['igamelogic_53',['IGameLogic',['../interface_game_1_1_logic_1_1_i_game_logic.html',1,'Game::Logic']]],
  ['igamemodel_54',['IGameModel',['../interface_game_1_1_model_1_1_i_game_model.html',1,'Game::Model']]],
  ['init_55',['Init',['../class_game_1_1_tests_1_1_logic_test.html#ac2f7b5b8d125828faea3862c51ccc3e0',1,'Game::Tests::LogicTest']]],
  ['initializecomponent_56',['InitializeComponent',['../class_game___wpf_1_1_app.html#ac75c37e3177091ca7b9fd0dfa52175aa',1,'Game_Wpf.App.InitializeComponent()'],['../class_game___wpf_1_1_app.html#ac75c37e3177091ca7b9fd0dfa52175aa',1,'Game_Wpf.App.InitializeComponent()'],['../class_game___wpf_1_1_end_window.html#ab4be971936e759ccc1fe1192bc0c1e8f',1,'Game_Wpf.EndWindow.InitializeComponent()'],['../class_game___wpf_1_1_end_window.html#ab4be971936e759ccc1fe1192bc0c1e8f',1,'Game_Wpf.EndWindow.InitializeComponent()'],['../class_game___wpf_1_1_main_window.html#aaf545008a7ba007d5e1b6960e6a98d2a',1,'Game_Wpf.MainWindow.InitializeComponent()'],['../class_game___wpf_1_1_main_window.html#aaf545008a7ba007d5e1b6960e6a98d2a',1,'Game_Wpf.MainWindow.InitializeComponent()'],['../class_game___wpf_1_1_start_window.html#abb5eddc6b9f111342b6e616580f43803',1,'Game_Wpf.StartWindow.InitializeComponent()'],['../class_game___wpf_1_1_start_window.html#abb5eddc6b9f111342b6e616580f43803',1,'Game_Wpf.StartWindow.InitializeComponent()']]],
  ['isdead_57',['IsDead',['../class_game_1_1_model_1_1_bullet.html#a5bf86d633e63dcd2605037644dc27b48',1,'Game::Model::Bullet']]],
  ['ishit_58',['IsHit',['../class_game_1_1_model_1_1_bullet.html#a52b336da505c601417675e5fbd718a93',1,'Game::Model::Bullet']]]
];

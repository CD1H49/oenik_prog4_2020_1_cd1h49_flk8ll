var interface_game___wpf_1_1_i_game_graphics_renderer =
[
    [ "DrawAll", "interface_game___wpf_1_1_i_game_graphics_renderer.html#af4a31d7f45e919ce07af483a829cd9db", null ],
    [ "DrawBackground", "interface_game___wpf_1_1_i_game_graphics_renderer.html#a519f3269649f1a71f906efb2de608686", null ],
    [ "DrawBullets", "interface_game___wpf_1_1_i_game_graphics_renderer.html#a91b4396fb6a3d55ad3daf489d66eef75", null ],
    [ "DrawGhosts", "interface_game___wpf_1_1_i_game_graphics_renderer.html#a59b0c2e983769775d8f2b7d6853270cb", null ],
    [ "DrawLifes", "interface_game___wpf_1_1_i_game_graphics_renderer.html#a7dd765b922b05d27005eaeed5f3fc1c5", null ],
    [ "DrawMag", "interface_game___wpf_1_1_i_game_graphics_renderer.html#a488c2496718e65ac4ce8b3b9ba79b057", null ],
    [ "DrawPilot", "interface_game___wpf_1_1_i_game_graphics_renderer.html#aa53d9dbb9b678b24db1322e53187d089", null ],
    [ "DrawScore", "interface_game___wpf_1_1_i_game_graphics_renderer.html#aeafca78c3168a2ef972355f9ca9f6ebb", null ],
    [ "GetBrush", "interface_game___wpf_1_1_i_game_graphics_renderer.html#a49bce081a2d16eb855393e0b7cb4d77f", null ]
];
var dir_26939d63feb366eb48260de57e987c54 =
[
    [ "Game.Controller", "dir_5e299305a9fe27ab1e6d9bcaff85418b.html", "dir_5e299305a9fe27ab1e6d9bcaff85418b" ],
    [ "Game.GraphicsRenderer", "dir_ed3e790c385652ed2d82dc3a444ee7eb.html", "dir_ed3e790c385652ed2d82dc3a444ee7eb" ],
    [ "Game.Logic", "dir_867c2e4c28040ab14abdd0714420aaad.html", "dir_867c2e4c28040ab14abdd0714420aaad" ],
    [ "Game.Model", "dir_b36e835b7d0d920498d85e6f763d0d1d.html", "dir_b36e835b7d0d920498d85e6f763d0d1d" ],
    [ "Game.Tests", "dir_67eb1e787b024232fe8f66b3f8be3187.html", "dir_67eb1e787b024232fe8f66b3f8be3187" ],
    [ "Game_Wpf", "dir_4283710229e225b67af4b4e34fc56bfe.html", "dir_4283710229e225b67af4b4e34fc56bfe" ],
    [ "GameControl", "dir_9019b945205e9c08d1b98880f261c904.html", "dir_9019b945205e9c08d1b98880f261c904" ],
    [ "Model.Logic", "dir_b71cc03e90572a8dd4fc2b14126e621f.html", "dir_b71cc03e90572a8dd4fc2b14126e621f" ]
];
var hierarchy =
[
    [ "Application", null, [
      [ "Game_Wpf.App", "class_game___wpf_1_1_app.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "Game.Control.GameControl", "class_game_1_1_control_1_1_game_control.html", null ],
      [ "Game.Controller.GameControl", "class_game_1_1_controller_1_1_game_control.html", null ],
      [ "Game_Wpf.GameController", "class_game___wpf_1_1_game_controller.html", null ],
      [ "Game_WpfApp.GameControl", "class_game___wpf_app_1_1_game_control.html", null ]
    ] ],
    [ "Game.Model.GameItem", "class_game_1_1_model_1_1_game_item.html", [
      [ "Game.Model.Bullet", "class_game_1_1_model_1_1_bullet.html", null ],
      [ "Game.Model.Ghost", "class_game_1_1_model_1_1_ghost.html", null ],
      [ "Game.Model.Pilot", "class_game_1_1_model_1_1_pilot.html", null ]
    ] ],
    [ "IComponentConnector", null, [
      [ "Game_Wpf.StartWindow", "class_game___wpf_1_1_start_window.html", null ]
    ] ],
    [ "Game_Wpf.IGameGraphicsRenderer", "interface_game___wpf_1_1_i_game_graphics_renderer.html", [
      [ "Game_Wpf.GraphicsRenderer", "class_game___wpf_1_1_graphics_renderer.html", null ]
    ] ],
    [ "Game.GraphicsRenderer.IGameGraphicsRenderer", "interface_game_1_1_graphics_renderer_1_1_i_game_graphics_renderer.html", [
      [ "Game.GraphicsRenderer.GraphicsRenderer", "class_game_1_1_graphics_renderer_1_1_graphics_renderer.html", null ]
    ] ],
    [ "Game.Logic.IGameLogic", "interface_game_1_1_logic_1_1_i_game_logic.html", [
      [ "Game.Logic.GameLogic", "class_game_1_1_logic_1_1_game_logic.html", null ]
    ] ],
    [ "Game.Model.IGameModel", "interface_game_1_1_model_1_1_i_game_model.html", [
      [ "Game.Model.GameModel", "class_game_1_1_model_1_1_game_model.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Game.Tests.LogicTest", "class_game_1_1_tests_1_1_logic_test.html", null ],
    [ "Window", null, [
      [ "Game_Wpf.EndWindow", "class_game___wpf_1_1_end_window.html", null ],
      [ "Game_Wpf.MainWindow", "class_game___wpf_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "Game_Wpf.StartWindow", "class_game___wpf_1_1_start_window.html", null ]
    ] ]
];
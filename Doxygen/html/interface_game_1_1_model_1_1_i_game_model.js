var interface_game_1_1_model_1_1_i_game_model =
[
    [ "Background", "interface_game_1_1_model_1_1_i_game_model.html#af130c8b889f1add23d8c10ed88e02c2f", null ],
    [ "Bullets", "interface_game_1_1_model_1_1_i_game_model.html#aa62c0b2b48d074dd1d5a9eff54988535", null ],
    [ "BulletStance", "interface_game_1_1_model_1_1_i_game_model.html#a11408415ca1dd9c0aa2416c4b79bfa70", null ],
    [ "CurrentLevel", "interface_game_1_1_model_1_1_i_game_model.html#a2b62fb696427f335aa8ad31c56fc3d82", null ],
    [ "FireRate", "interface_game_1_1_model_1_1_i_game_model.html#a91385949b196f99f9c38acb73a0b8d0c", null ],
    [ "Ghosts", "interface_game_1_1_model_1_1_i_game_model.html#a743e9254f9bbe814d421157a7a738002", null ],
    [ "GhostStance", "interface_game_1_1_model_1_1_i_game_model.html#accf1580034ddd3c223b6cd7bae7ebe57", null ],
    [ "HighScore", "interface_game_1_1_model_1_1_i_game_model.html#a79ecfb3e52b01343fea5e67e6149ded7", null ],
    [ "Lifes", "interface_game_1_1_model_1_1_i_game_model.html#ac9045892e63a266d5017bf42cd7ac518", null ],
    [ "Pilot", "interface_game_1_1_model_1_1_i_game_model.html#a69bced142c5bd6795d0b604e4d13141f", null ],
    [ "PilotStance", "interface_game_1_1_model_1_1_i_game_model.html#ab0e8d59330f3986cd8e4ac041b6ccfae", null ],
    [ "Score", "interface_game_1_1_model_1_1_i_game_model.html#ac73978f4a7d4cd027ea9fff87dff9be1", null ],
    [ "TempFireRate", "interface_game_1_1_model_1_1_i_game_model.html#a928f503439425adeb9da21739adfa4a6", null ]
];
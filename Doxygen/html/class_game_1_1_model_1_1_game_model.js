var class_game_1_1_model_1_1_game_model =
[
    [ "GameModel", "class_game_1_1_model_1_1_game_model.html#a2786fe651c1fe83c307abf55dc4f7f17", null ],
    [ "Background", "class_game_1_1_model_1_1_game_model.html#aa951194d133009cba121b1de29bfe82f", null ],
    [ "Bullets", "class_game_1_1_model_1_1_game_model.html#af21a5a2702fccddc97c0e5877299d3e4", null ],
    [ "BulletStance", "class_game_1_1_model_1_1_game_model.html#a1a5553aae37780a33a0b292e201ced2a", null ],
    [ "CurrentLevel", "class_game_1_1_model_1_1_game_model.html#a3da4230a9b3f2e879bab36c61d7e062f", null ],
    [ "FireRate", "class_game_1_1_model_1_1_game_model.html#af9bc8bdaea6ad74fd92a7c5bfffe5720", null ],
    [ "Ghosts", "class_game_1_1_model_1_1_game_model.html#ad562b9d21ef168e87b28043e4dd8e0b3", null ],
    [ "GhostStance", "class_game_1_1_model_1_1_game_model.html#a8586e15fec7666909ac949a743664b30", null ],
    [ "HighScore", "class_game_1_1_model_1_1_game_model.html#a2207dccfe956333b8e0f09a7c9b7418f", null ],
    [ "Lifes", "class_game_1_1_model_1_1_game_model.html#ab02e21b10bfbc4e7e82a06ba0a5a3577", null ],
    [ "Pilot", "class_game_1_1_model_1_1_game_model.html#a91c4174afdec2708cf481cbdabb025a8", null ],
    [ "PilotStance", "class_game_1_1_model_1_1_game_model.html#a3805865678181ce9e3ace00beb205dc6", null ],
    [ "Score", "class_game_1_1_model_1_1_game_model.html#aa98cb124a2b867cdc691d11004e843b5", null ],
    [ "TempFireRate", "class_game_1_1_model_1_1_game_model.html#afafd8f51ec5a44144b3b2553ac65b874", null ]
];
var namespaces_dup =
[
    [ "NUnit 3.11 - October 6, 2018", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md2", [
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md1", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md3", null ]
    ] ],
    [ "NUnit 3.10.1 - March 12, 2018", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md4", null ],
    [ "NUnit 3.10 - March 12, 2018", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md5", [
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md6", null ]
    ] ],
    [ "NUnit 3.9 - November 10, 2017", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md7", [
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md8", null ]
    ] ],
    [ "NUnit 3.8.1 - August 28, 2017", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md9", [
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md10", null ]
    ] ],
    [ "NUnit 3.8 - August 27, 2017", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md11", [
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md12", null ]
    ] ],
    [ "NUnit 3.7.1 - June 6, 2017", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md13", [
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md14", null ]
    ] ],
    [ "NUnit 3.7 - May 29, 2017", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md15", [
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md16", null ]
    ] ],
    [ "NUnit 3.6.1 - February 26, 2017", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md17", [
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md18", null ]
    ] ],
    [ "NUnit 3.6 - January 9, 2017", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md19", [
      [ "Framework", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md20", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md21", null ]
    ] ],
    [ "NUnit 3.5 - October 3, 2016", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md22", [
      [ "Framework", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md23", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md24", null ]
    ] ],
    [ "NUnit 3.4.1 - June 30, 2016", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md25", [
      [ "Console Runner", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md26", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md27", null ]
    ] ],
    [ "NUnit 3.4 - June 25, 2016", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md28", [
      [ "Framework", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md29", null ],
      [ "Engine", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md30", null ],
      [ "Console Runner", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md31", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md32", null ]
    ] ],
    [ "NUnit 3.2.1 - April 19, 2016", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md33", [
      [ "Framework", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md34", null ],
      [ "Engine", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md35", null ],
      [ "Console Runner", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md36", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md37", null ]
    ] ],
    [ "NUnit 3.2 - March 5, 2016", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md38", [
      [ "Framework", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md39", null ],
      [ "Engine", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md40", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md41", null ]
    ] ],
    [ "NUnit 3.0.1 - December 1, 2015", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md42", [
      [ "Console Runner", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md43", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md44", null ]
    ] ],
    [ "NUnit 3.0.0 Final Release - November 15, 2015", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md45", [
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md46", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate 3 - November 13, 2015", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md47", [
      [ "Engine", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md48", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md49", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate 2 - November 8, 2015", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md50", [
      [ "Engine", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md51", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md52", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate - November 1, 2015", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md53", [
      [ "Framework", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md54", null ],
      [ "NUnitLite", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md55", null ],
      [ "Engine", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md56", null ],
      [ "Console Runner", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md57", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md58", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 5 - October 16, 2015", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md59", [
      [ "Framework", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md60", null ],
      [ "Engine", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md61", null ],
      [ "Console Runner", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md62", [
        [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md63", null ]
      ] ]
    ] ],
    [ "NUnit 3.0.0 Beta 4 - August 25, 2015", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md64", [
      [ "Framework", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md65", null ],
      [ "Engine", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md66", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md67", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 3 - July 15, 2015", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md68", [
      [ "Framework", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md69", null ],
      [ "Engine", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md70", null ],
      [ "Console", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md71", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md72", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 2 - May 12, 2015", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md73", [
      [ "Framework", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md74", null ],
      [ "Engine", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md75", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md76", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 1 - March 25, 2015", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md77", [
      [ "General", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md78", null ],
      [ "Framework", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md79", null ],
      [ "Engine", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md80", null ],
      [ "Console", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md81", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md82", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 5 - January 30, 2015", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md83", [
      [ "General", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md84", null ],
      [ "Framework", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md85", null ],
      [ "Engine", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md86", null ],
      [ "Console", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md87", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md88", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 4 - December 30, 2014", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md89", [
      [ "Framework", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md90", null ],
      [ "Engine", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md91", null ],
      [ "Console", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md92", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md93", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 3 - November 29, 2014", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md94", [
      [ "Breaking Changes", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md95", null ],
      [ "Framework", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md96", null ],
      [ "Engine", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md97", null ],
      [ "Console", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md98", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md99", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 2 - November 2, 2014", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md100", [
      [ "Breaking Changes", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md101", null ],
      [ "General", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md102", null ],
      [ "New Features", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md103", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md104", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 1 - September 22, 2014", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md105", [
      [ "Breaking Changes", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md106", null ],
      [ "General", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md107", null ],
      [ "New Features", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md108", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md109", null ],
      [ "Console Issues Resolved (Old nunit-console project, now combined with nunit)", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md110", null ]
    ] ],
    [ "NUnit 2.9.7 - August 8, 2014", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md111", [
      [ "Breaking Changes", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md112", null ],
      [ "New Features", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md113", null ],
      [ "Issues Resolved", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md114", null ]
    ] ],
    [ "NUnit 2.9.6 - October 4, 2013", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md115", [
      [ "Main Features", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md116", null ],
      [ "Bug Fixes", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md117", null ],
      [ "Bug Fixes in 2.9.6 But Not Listed Here in the Release", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md118", null ]
    ] ],
    [ "NUnit 2.9.5 - July 30, 2010", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md119", [
      [ "Bug Fixes", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md120", null ]
    ] ],
    [ "NUnit 2.9.4 - May 4, 2010", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md121", [
      [ "Bug Fixes", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md122", null ]
    ] ],
    [ "NUnit 2.9.3 - October 26, 2009", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md123", [
      [ "Main Features", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md124", null ],
      [ "Bug Fixes", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md125", null ]
    ] ],
    [ "NUnit 2.9.2 - September 19, 2009", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md126", [
      [ "Main Features", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md127", null ],
      [ "Bug Fixes", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md128", null ]
    ] ],
    [ "NUnit 2.9.1 - August 27, 2009", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md129", [
      [ "General", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md130", null ],
      [ "Bug Fixes", "md__c_1__users__patrik__documents_oenik_prog4_2020_1_cd1h49_flk8ll__o_e_n_i_k__p_r_o_g4_2020_1__44e81237c20da60235fc844eec133e96.html#autotoc_md131", null ]
    ] ],
    [ "Game", "namespace_game.html", "namespace_game" ],
    [ "Game_Wpf", "namespace_game___wpf.html", "namespace_game___wpf" ],
    [ "Game_WpfApp", "namespace_game___wpf_app.html", null ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", null ]
];
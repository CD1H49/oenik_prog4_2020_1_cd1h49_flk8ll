﻿// <copyright file="IGameLogic.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// IGameLogic interface. Contains the game logic.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// MoveBackground interface.
        /// </summary>
        void MoveBackground();

        /// <summary>
        /// MoveBackGroundOneStep interface.
        /// </summary>
        void MoveBackGroundOneStep(int i);

        /// <summary>
        /// MoveGhosts interface.
        /// </summary>
        void MoveGhosts();

        /// <summary>
        /// MoveBullet interface.
        /// </summary>
        void MoveBullet();

        /// <summary>
        /// MouseControl interface.
        /// </summary>
        void MouseControl(Point point);

        /// <summary>
        /// KeyboardControl interface.
        /// </summary>
        /// <param name="k">Key object.</param>
        void KeyboardControl(Key k, bool paused);

        /// <summary>
        /// AddBullet interface.
        /// </summary>
        void AddBullet(bool paused);

        /// <summary>
        /// ChangeFireRate interface.
        /// </summary>
        void ChangeFireRate();

        /// <summary>
        /// DeleteBullet interface.
        /// </summary>
        void DeleteBullet();

        /// <summary>
        /// Load interface.
        /// </summary>
        void Load();

        /// <summary>
        /// Save interface.
        /// </summary>
        void Save();
    }
}

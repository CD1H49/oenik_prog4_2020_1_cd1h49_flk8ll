﻿// <copyright file="GameLogic.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game.Logic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using Game.Model;

    /// <summary>
    /// GameLogic class is responsible for movements and adding bullet or removing items.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        private readonly Random r = new Random();
        private readonly GameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// GameLogic constructor.
        /// </summary>
        /// <param name="model"> set up gameLogic.</param>
        public GameLogic(GameModel model)
        {
            this.model = model;
        }

        /// <summary>
        /// event that refreshes the screen.
        /// </summary>
        public event EventHandler RefreshScreen;

        /// <summary>
        /// this method moves the backgorund.
        /// </summary>
        public void MoveBackground()
        {
            for (int i = 0; i < this.model.Background.Length; i++)
            {
                this.MoveBackGroundOneStep(i);
            }

            this.RefreshScreen?.Invoke(null, EventArgs.Empty);
        }

        /// <summary>
        /// MoveBackGroundOneStep was created to help the unit test.
        /// </summary>
        /// <param name="i">onestep moving.</param>
        public void MoveBackGroundOneStep(int i)
        {
            if (this.model.Background[i].X <= -Config.ResolutionWidth && i == 0)
            {
                this.model.Background[i].X = 0;
            }

            if (this.model.Background[i].X <= 0 && i == 1)
            {
                this.model.Background[i].X = Config.ResolutionWidth - 1;
            }

            this.model.Background[i].X -= Config.BackgroundSpeed * ((double)(this.model.CurrentLevel - 1) / 4);
        }

        /// <summary>
        /// this method moves the ghost objects.
        /// </summary>
        public void MoveGhosts()
        {
            foreach (var ghost in this.model.Ghosts)
            {
                if (ghost.GetPoint.X < 0)
                {
                    ghost.SetXY(Config.ResolutionWidth, this.r.Next(0, (int)Config.ResolutionHeight - 300));
                }

                if (ghost.Area.IntersectsWith(this.model.Pilot.Area))
                {
                    ghost.SetXY(Config.ResolutionWidth, this.r.Next(0, (int)Config.ResolutionHeight - 300));
                    this.model.Lifes--;
                }

                foreach (var item in this.model.Bullets.Where(x => x.Area.IntersectsWith(ghost.Area) && !x.IsHit))
                {
                    item.IsHit = true;
                    ghost.SetXY(Config.ResolutionWidth, this.r.Next(0, (int)Config.ResolutionHeight - 300));

                    if ((this.model.Score + 2) % (5 + 1) == 0)
                    {
                        this.model.CurrentLevel += 0.5;
                    }

                    this.model.Score++;
                }

                ghost.ChangePointX(Config.GhostSpeed * ((double)this.model.CurrentLevel / 2));
            }
        }

        /// <summary>
        /// moves the bullets.
        /// </summary>
        public void MoveBullet()
        {
            foreach (var bullet in this.model.Bullets)
            {
                if (bullet.Area.IntersectsWith(this.model.Pilot.Area))
                {
                    bullet.SetXY(Config.ResolutionWidth, 0);
                    this.model.Lifes--;
                }

                if (!bullet.IsHit)
                {
                    bullet.ChangePointX(Config.BulletSpeed);
                }

                if (bullet.IsHit && bullet.IsDead)
                {
                    bullet.SetXY(Config.ResolutionWidth + 1, 0);
                }
            }
        }

        /// <summary>
        /// you can control by mouse the pilot.
        /// </summary>
        /// <param name="point">control pilot with mouse.</param>
        public void MouseControl(Point point)
        {
            this.model.Pilot.SetXY(point.X, point.Y);
        }

        /// <summary>
        /// key control .
        /// </summary>
        /// <param name="k">key.</param>
        public void KeyboardControl(Key k, bool paused)
        {
            switch (k)
            {
                case Key.Left:
                    this.model.Pilot.ChangePointX(-20);
                    break;
                case Key.Right:
                    this.model.Pilot.ChangePointX(+20);
                    break;
                case Key.Up:
                    this.model.Pilot.ChangePointY(-20);
                    break;
                case Key.Down:
                    this.model.Pilot.ChangePointY(+20);
                    break;
                case Key.Space:
                    this.AddBullet(paused);
                    break;
            }
        }

        /// <summary>
        /// Add bullet to the bullet list when firing.
        /// </summary>
        public void AddBullet(bool paused)
        {
            if (paused)
            {
                if (Math.Round(this.model.FireRate) > 0)
                {
                    Config.BulletFireSound.Play();
                    this.model.Bullets.Add(new Bullet(
                        this.model.Pilot.GetPoint.X + 20 + Config.PilotWidth,
                        this.model.Pilot.GetPoint.Y + (Config.PilotHeight / 2)));
                    this.model.FireRate--;
                }
            }
        }

        /// <summary>
        /// ChangeFireRate method Updates firerate when leveling up.
        /// </summary>
        public void ChangeFireRate()
        {
            if (this.model.FireRate >= 5)
            {
                this.model.FireRate = 5;
            }
            else
            {
                this.model.FireRate += 0.02 * (this.model.CurrentLevel / 2);
            }
        }

        /// <summary>
        /// Delete bullet from the bullet list.
        /// </summary>
        public void DeleteBullet()
        {
            foreach (var item in this.model.Bullets.ToList())
            {
                if (item.GetPoint.X > Config.ResolutionWidth)
                {
                    _ = this.model.Bullets.Remove(item);
                }
            }
        }

        /// <summary>
        /// Load the current score from text file.
        /// </summary>
        public void Load()
        {
            try
            {
                this.model.HighScore = int.Parse(File.ReadAllText(Config.SavePath).ToString());
            }
            catch (FileNotFoundException)
            {
                this.model.HighScore = 0;
            }
        }

        /// <summary>
        /// Write into file the current highscore.
        /// </summary>
        public void Save()
        {
            if (this.model.Score > this.model.HighScore)
            {
                StreamWriter sw = new StreamWriter(Config.SavePath);
                sw.WriteLine(this.model.Score.ToString());
                sw.Close();
            }
        }
    }
}

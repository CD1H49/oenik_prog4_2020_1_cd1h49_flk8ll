﻿// <copyright file="GameControl.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game.Controller
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using Game.Model;
    using Game.Logic;
    using Game.Renderer;
    using Oenik_Prog4_2020_1_CD1H49_Flk8LL;

   public class GameControl : FrameworkElement
    {
        private GameModel model;
        private GameLogic logic;
        private GraphicsRenderer renderer;
        private DispatcherTimer tickTimer;
        private Window win;

        public GameControl()
        {
            this.Loaded += this.GameControl_Loaded;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                if (this.tickTimer.IsEnabled)
                {
                    drawingContext.DrawDrawing(this.renderer.DrawAll());
                }
                else
                {
                    drawingContext.DrawDrawing(this.renderer.DrawButton());
                }
            }
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.model = new GameModel();
            this.logic = new GameLogic(this.model);
            this.renderer = new GraphicsRenderer(this.model);

            this.win = Window.GetWindow(this);
            if (this.win != null)
            {
                Config.GameStartSound.Play();
                this.tickTimer = new DispatcherTimer();
                this.tickTimer.Interval = TimeSpan.FromMilliseconds(17);
                this.tickTimer.Tick += this.TickTimer_Tick;
                this.tickTimer.Start();
            }

            this.win.MouseMove += this.Win_MouseMove;
            this.win.KeyDown += this.Win_KeyDown;
            this.win.MouseDown += this.Win_MouseDown;

            this.logic.Load();
            this.logic.RefreshScreen += (obj, args) => this.InvalidateVisual();
            this.InvalidateVisual();
        }

        private void Win_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.logic.AddBullet(this.tickTimer.IsEnabled);
        }

        private void Win_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.P)
            {
                this.tickTimer.IsEnabled = !this.tickTimer.IsEnabled;
                this.InvalidateVisual();
            }
            else
            {
                this.logic.KeyboardControl(e.Key, this.tickTimer.IsEnabled);
            }
        }

        private void Win_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            this.logic.MouseControl(e.GetPosition(this));
        }

        private void TickTimer_Tick(object sender, EventArgs e)
        {
            if (this.model.Lifes != 0)
            {
                this.logic.MoveBackground();
                this.logic.MoveGhosts();
                this.logic.DeleteBullet();
                this.logic.MoveBullet();
                this.logic.ChangeFireRate();
            }
            else
            {
                this.tickTimer.Stop();
                this.logic.Save();
                EndWindow end = new EndWindow();
                end.Show();
                this.win.Close();
            }
        }
    }
}

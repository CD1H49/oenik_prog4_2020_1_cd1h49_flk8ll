﻿// <copyright file="MainWindow.xaml.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game_Wpf
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }
    }
}
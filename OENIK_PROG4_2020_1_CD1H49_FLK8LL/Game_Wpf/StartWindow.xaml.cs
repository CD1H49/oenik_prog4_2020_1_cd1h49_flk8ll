﻿// <copyright file="StartWindow.xaml.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game_Wpf
{
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using Game.Model;

    /// <summary>
    /// Interaction logic for StartWindow.xaml.
    /// </summary>
    public partial class StartWindow : Window
    {
        private bool isDraggingCoin;
        private Point clickPosition;
        private TranslateTransform originTT;
        private int highScore;

        public StartWindow()
        {
            this.InitializeComponent();
            this.Load();
            this.InvalidateVisual();
        }

        public void Load()
        {
            try
            {
                this.highScore = int.Parse(File.ReadAllText(Config.SavePath));
                this.highscoreText.Text = "HighScore: " + this.highScore.ToString();
            }
            catch (FileNotFoundException)
            {
                this.highScore = 0;
            }
        }

        private void Canvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.isDraggingCoin = false;
            var draggable = sender as Image;
            draggable.ReleaseMouseCapture();

            if (this.clickPosition.X <= 378 && this.clickPosition.X >= 200 && this.clickPosition.Y <= 885 && this.clickPosition.Y >= 600)
            {
                Window window = Window.GetWindow(this);
                MainWindow win = new MainWindow();
                win.Show();
                window.Close();
            }
        }

        private void Canvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var draggableControl = sender as Image;
            this.originTT = draggableControl.RenderTransform as TranslateTransform ?? new TranslateTransform();
            this.isDraggingCoin = true;
            this.clickPosition = e.GetPosition(this);
            draggableControl.CaptureMouse();
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            var draggableControl = sender as Image;
            if (this.isDraggingCoin && draggableControl != null)
            {
                Point currentPosition = e.GetPosition(this);
                var transform = draggableControl.RenderTransform as TranslateTransform ?? new TranslateTransform();
                transform.X = this.originTT.X + (currentPosition.X - this.clickPosition.X);
                transform.Y = this.originTT.Y + (currentPosition.Y - this.clickPosition.Y);
                draggableControl.RenderTransform = new TranslateTransform(transform.X, transform.Y);
            }
        }
    }
}

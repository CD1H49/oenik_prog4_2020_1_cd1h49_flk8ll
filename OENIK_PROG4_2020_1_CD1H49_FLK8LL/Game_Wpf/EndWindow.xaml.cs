﻿// <copyright file="EndWindow.xaml.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game_Wpf
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for EndWindow.xaml.
    /// </summary>
    public partial class EndWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EndWindow"/> class.
        /// </summary>
        public EndWindow()
        {
            this.InitializeComponent();
        }

        private void SpaceQuit(object sender, RoutedEventArgs e)
        {
            Window w = Window.GetWindow(this);
            w.Close();
        }
    }
}

﻿// <copyright file="App.xaml.cs" company="CD1H49_FLK8LL">
// Copyright (c) CD1H49_FLK8LL. All rights reserved.
// </copyright>

namespace Game_Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
    }
}

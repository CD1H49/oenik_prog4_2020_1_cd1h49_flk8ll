﻿// <copyright file="IGameGraphicsRenderer.cs" company="CD1H49_FLK8LL">
// Copyright (c) CD1H49_FLK8LL. All rights reserved.
// </copyright>

namespace Game_Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// IGameGraphicsRenderer interface contains GraphicsRenderer class required methods, properties.
    /// </summary>
    public interface IGameGraphicsRenderer
    {
        /// <summary>
        /// DrawAll() method.
        /// </summary>
        /// <returns>ctx.</returns>
        Drawing DrawAll();

        /// <summary>
        /// DrawPilot() method.
        /// </summary>
        /// <returns>ctx.</returns>
        Drawing DrawPilot();

        /// <summary>
        /// DrawGhosts() method.
        /// </summary>
        /// <returns>ctx.</returns>
        List<Drawing> DrawGhosts();

        /// <summary>
        /// DrawBullets() method.
        /// </summary>
        /// <returns>ctx.</returns>
        List<Drawing> DrawBullets();

        /// <summary>
        /// DrawScore() method.
        /// </summary>
        /// <returns>ctx.</returns>
        Drawing DrawScore();

        /// <summary>
        /// DrawMag() method.
        /// </summary>
        /// <returns>ctx.</returns>
        Drawing DrawMag();

        /// <summary>
        /// DrawLifes() method.
        /// </summary>
        /// <returns>ctx.</returns>
        List<Drawing> DrawLifes();

        /// <summary>
        /// DrawBackground() method.
        /// </summary>
        /// <returns>ctx.</returns>
        Drawing[] DrawBackground();

        /// <summary>
        /// GetBrush method.
        /// </summary>
        /// <returns>ctx.</returns>
        Brush GetBrush(string fname);
    }
}

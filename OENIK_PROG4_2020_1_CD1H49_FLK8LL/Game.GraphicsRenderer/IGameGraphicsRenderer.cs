﻿// <copyright file="IGameGraphicsRenderer.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game.GraphicsRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// IGameGraphicsRenderer interface contains GraphicsRenderer class required methods, properties.
    /// </summary>
    public interface IGameGraphicsRenderer
    {
        Drawing DrawAll();

        Drawing DrawPilot();

        List<Drawing> DrawGhosts();

        List<Drawing> DrawBullets();

        Drawing DrawScore();

        Drawing DrawMag();

        List<Drawing> DrawLifes();

        Drawing[] DrawBackground();

        Brush GetBrush(string fname);
    }
}

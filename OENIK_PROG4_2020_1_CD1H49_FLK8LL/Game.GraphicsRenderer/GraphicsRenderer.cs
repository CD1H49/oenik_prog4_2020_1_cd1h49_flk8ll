﻿// <copyright file="GraphicsRenderer.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game.GraphicsRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Media.Imaging;
    using Game.Model;

    /// <summary>
    /// Build the graphics of the game.
    /// </summary>
    public class GraphicsRenderer : IGameGraphicsRenderer
    {
        private GameModel model;

        /// <summary>
        /// Backgrounds array.
        /// </summary>
        private Drawing[] backgrounds;

        /// <summary>
        /// Pilot old drawing object.
        /// </summary>
        private Drawing oldPilot;

        /// <summary>
        /// Ghosts old drawing list.
        /// </summary>
        private List<Drawing> oldGhosts;

        /// <summary>
        /// Bullets old drawing list.
        /// </summary>
        private List<Drawing> oldBullets;

        /// <summary>
        /// Lifes old drawing list.
        /// </summary>
        private List<Drawing> lifes;

        /// <summary>
        /// Temporary life number.
        /// </summary>
        private int tempLifeN;
        private int tempScore;
        private double tempMag;

        private Drawing coin;
        private Drawing score;
        private Typeface font;
        private Drawing mag;
        private Drawing pauseButton;

        private Dictionary<string, Brush> brushes = new Dictionary<string, Brush>();

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphicsRenderer"/> class.
        /// </summary>
        /// <param name="model">GameModel object.</param>
        public GraphicsRenderer(GameModel model)
        {
            this.font = new Typeface("Arial");
            this.model = model;
            this.tempLifeN = this.model.Lifes;
            this.tempScore = this.model.Score;
            this.tempMag = this.model.FireRate;

            Geometry coin = new RectangleGeometry(new Rect(Config.CoinPlaceX, Config.CoinPlaceY, Config.CoinSizeWH, Config.CoinSizeWH));
            this.coin = new GeometryDrawing(this.GetCoinBrush(), null, coin);

            Geometry button = new RectangleGeometry(new Rect((Config.ResolutionWidth / 2) - Config.PauseButtonW, Config.ResolutionHeight / 2, Config.PauseButtonW, Config.PauseButtonW));
            this.pauseButton = new GeometryDrawing(this.GetPauseButtonBrush(), null, button);
        }

        /// <summary>
        /// Draw the models.
        /// </summary>
        /// <returns>Drawing object.</returns>
        public Drawing DrawAll()
        {
            DrawingGroup dg = new DrawingGroup();

            dg.Children.Add(this.DrawBackground()[0]);
            dg.Children.Add(this.DrawBackground()[1]);

            dg.Children.Add(this.DrawPilot());

            this.DrawGhosts().ForEach(x => dg.Children.Add(x));
            this.DrawBullets().ForEach(x => dg.Children.Add(x));

            dg.Children.Add(this.DrawScore());
            dg.Children.Add(this.coin);

            dg.Children.Add(this.DrawMag());
            this.DrawLifes().ForEach(x => dg.Children.Add(x));

            return dg;
        }

        public Drawing DrawButton()
        {
            DrawingGroup dg = new DrawingGroup();

            dg.Children.Add(this.DrawBackground()[0]);
            dg.Children.Add(this.DrawBackground()[1]);

            dg.Children.Add(this.DrawPilot());

            this.DrawGhosts().ForEach(x => dg.Children.Add(x));
            this.DrawBullets().ForEach(x => dg.Children.Add(x));

            dg.Children.Add(this.DrawScore());
            dg.Children.Add(this.coin);

            dg.Children.Add(this.DrawMag());

            dg.Children.Add(this.pauseButton);
            this.DrawLifes().ForEach(x => dg.Children.Add(x));

            return dg;
        }

        /// <summary>
        /// Create the background geometries.
        /// </summary>
        /// <returns>Drawing array.</returns>
        public Drawing[] DrawBackground()
        {
            if (this.backgrounds == null)
            {
                this.backgrounds = new Drawing[2];
            }

            for (int i = 0; i < 2; i++)
            {
                Geometry g = new RectangleGeometry(new Rect(this.model.Background[i].X, this.model.Background[i].Y, Config.ResolutionWidth, Config.ResolutionHeight));
                this.backgrounds[i] = new GeometryDrawing(this.GetBackgroundBrush(), null, g);
            }

            return this.backgrounds;
        }

        /// <summary>
        /// Create the bullet object and add to the drawing list.
        /// </summary>
        /// <returns>Bullet drawing list.</returns>
        public List<Drawing> DrawBullets()
        {
            this.oldBullets = new List<Drawing>();

            foreach (var bullet in this.model.Bullets)
            {
                if (bullet.IsHit)
                {
                    Geometry b = new RectangleGeometry(
                    new Rect(bullet.GetPoint.X, bullet.GetPoint.Y, Config.GhostWidth, Config.GhostHeight));
                    this.oldBullets.Add(new GeometryDrawing(this.BulletBrush(bullet), null, b));
                }
                else
                {
                    Geometry g = new RectangleGeometry(
    new Rect(bullet.GetPoint.X, bullet.GetPoint.Y, Config.BulletWidth, Config.BulletHeight));
                    this.oldBullets.Add(new GeometryDrawing(this.BulletBrush(bullet), null, g));
                }

                if (bullet.BulletStance > 7 && bullet.IsHit)
                {
                    bullet.BulletStance = 1;
                    bullet.IsDead = true;
                }
            }

            return this.oldBullets;
        }

        /// <summary>
        /// Create the life object and add to the drawing list.
        /// </summary>
        /// <returns>Life drawing list.</returns>
        public List<Drawing> DrawLifes()
        {
            if (this.lifes == null || this.tempLifeN != this.model.Lifes)
            {
                this.lifes = new List<Drawing>();

                for (int i = 0; i < this.model.Lifes; i++)
                {
                    Geometry g =
                        new RectangleGeometry(new Rect(Config.LifePlaceX + (i * 70), Config.LifePlaceY, Config.LifeSizeWH, Config.LifeSizeWH));
                    this.lifes.Add(new GeometryDrawing(this.GetLifeBrush(), null, g));
                }
            }

            return this.lifes;
        }

        /// <summary>
        /// Create the score drawing object.
        /// </summary>
        /// <returns>Score drawing object.</returns>
        public Drawing DrawScore()
        {
            if (this.tempScore != this.model.Score || this.score == null)
            {
                FormattedText formattedText = new FormattedText(": " + this.model.Score.ToString(), CultureInfo.CurrentCulture, FlowDirection.LeftToRight, typeface: this.font, emSize: Config.ScoreTextSize, foreground: Config.FontColor);
                GeometryDrawing text =
                    new GeometryDrawing(
                        brush: null,
                        pen: new Pen(Config.FontColor, 2),
                        geometry: formattedText.BuildGeometry(new Point(Config.ScorePlaceX, Config.ScorePlaceY)));
                this.score = text;
            }

            return this.score;
        }

        /// <summary>
        /// Create the pilot drawing object.
        /// </summary>
        /// <returns>Pilot drawing object.</returns>
        public Drawing DrawPilot()
        {
            Geometry g = new RectangleGeometry(new Rect(this.model.Pilot.GetPoint.X, this.model.Pilot.GetPoint.Y, Config.PilotWidth, Config.PilotHeight));
            this.oldPilot = new GeometryDrawing(this.GetPilotBrush(), null, g);

            if (this.model.PilotStance > 3)
            {
                this.model.PilotStance = 1;
            }
            else
            {
                this.model.PilotStance += 0.2;
            }

            return this.oldPilot;
        }

        /// <summary>
        /// Create the ghost object and add to the drawing list.
        /// </summary>
        /// <returns>Ghost drawing list.</returns>
        public List<Drawing> DrawGhosts()
        {
            this.oldGhosts = new List<Drawing>();

            int counter = 0;
            foreach (var ghost in this.model.Ghosts)
            {
                Geometry g = new RectangleGeometry(new Rect(ghost.GetPoint.X, ghost.GetPoint.Y, Config.GhostWidth, Config.GhostHeight));
                this.oldGhosts.Add(new GeometryDrawing(this.GetGhostBrush(), null, g));

                counter++;
            }

            if (this.model.GhostStance > 4)
            {
                this.model.GhostStance = 1;
            }
            else
            {
                this.model.GhostStance += 0.2;
            }

            return this.oldGhosts;
        }

        /// <summary>
        /// DrawMag method is for drawing mags.
        /// </summary>
        /// <returns>Return mag.</returns>
        public Drawing DrawMag()
        {
            if (this.tempMag != Math.Round(this.model.FireRate) || this.mag == null)
            {
                Geometry g =
                    new RectangleGeometry(new Rect(Config.MagPlaceX, Config.MagPlaceY, Config.MagSizeW, Config.MagSizeH));
                this.mag = new GeometryDrawing(this.GetMagBrush(), null, g);
            }

            return this.mag;
        }

        /// <summary>
        /// Get back the image file as brush.
        /// </summary>
        /// <param name="fname">filename.</param>
        /// <returns>ImageBrush.</returns>
        public Brush GetBrush(string fname)
        {
            if (!this.brushes.ContainsKey(fname))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
                bmp.EndInit();
                ImageBrush ib = new ImageBrush(bmp);
                this.brushes[fname] = ib;
            }

            return this.brushes[fname];
        }

        private Brush GetPilotBrush()
        {
            return this.GetBrush($"Oenik_Prog4_2020_1_CD1H49_Flk8LL.Resource.pilot{Math.Round(this.model.PilotStance)}.png");
        }

        private Brush GetGhostBrush()
        {
            if (this.model.Score < 17)
            {
                return this.GetBrush($"Oenik_Prog4_2020_1_CD1H49_Flk8LL.Resource.slow_ghost{Math.Round(this.model.GhostStance)}.png");
            }
            else
            {
                return this.GetBrush($"Oenik_Prog4_2020_1_CD1H49_Flk8LL.Resource.fast_ghost{Math.Round(this.model.GhostStance)}.png");
            }
        }

        private Brush GetCoinBrush()
        {
            return this.GetBrush("Oenik_Prog4_2020_1_CD1H49_Flk8LL.Resource.coin.png");
        }

        private Brush GetPauseButtonBrush()
        {
            return this.GetBrush("Oenik_Prog4_2020_1_CD1H49_Flk8LL.Resource.pause.png");
        }

        private Brush GetBackgroundBrush()
        {
            return this.GetBrush($"Oenik_Prog4_2020_1_CD1H49_Flk8LL.Resource.background_{Math.Round((this.model.CurrentLevel / 4) % 3)}.png");
        }

        private Brush GetLifeBrush()
        {
            return this.GetBrush("Oenik_Prog4_2020_1_CD1H49_Flk8LL.Resource.life.png");
        }

        private Brush GetMagBrush()
        {
            return this.GetBrush($"Oenik_Prog4_2020_1_CD1H49_Flk8LL.Resource.bullet_{Math.Round(this.model.FireRate)}.png");
        }

        private Brush BulletBrush(Bullet bullet)
        {
            if (bullet.IsHit && !bullet.IsDead)
            {
                bullet.BulletStance += 0.2;
                return this.GetBrush($"Oenik_Prog4_2020_1_CD1H49_Flk8LL.Resource.dead_ghost{Math.Round(bullet.BulletStance)}.png");
            }
            else
            {
                return this.GetBrush("Oenik_Prog4_2020_1_CD1H49_Flk8LL.Resource.bullet.png");
            }
        }
    }
}

﻿// <copyright file="Config.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game.Model
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Created config class.
    /// </summary>
    public static class Config
    {
        /// <summary>
        /// Init ResolutionWidth.
        /// </summary>
        public static double ResolutionWidth = 1260;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double ResolutionHeight = 748;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double BackgroundSpeed = 10;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double PilotWidth = 175;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double PilotHeight = 175;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double BulletWidth = 100;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double BulletHeight = 100;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double BulletSpeed = 10;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double GhostSpeed = -5;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double GhostWidth = 156;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double GhostHeight = 156;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double Rad = 30;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static Brush FontColor = Brushes.White;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double ScorePlaceX = ResolutionWidth - 200;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double ScorePlaceY = 50;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double ScoreTextSize = 30;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double CoinSizeWH = 50;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double CoinPlaceX = ScorePlaceX - 52;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double CoinPlaceY = ScorePlaceY - 5;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double LifeSizeWH = 50;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double LifePlaceX = ResolutionWidth - 320;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double LifePlaceY = ResolutionHeight - 70;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double MagPlaceX = 50;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double MagPlaceY = ResolutionHeight - 100;

        /// <summary>
        /// Init ResolutionHeight.
        /// </summary>
        public static double MagSizeW = 200;

        /// <summary>
        /// Init Magsize.
        /// </summary>
        public static double MagSizeH = 50;

        /// <summary>
        /// PauseButton init.
        /// </summary>
        public static double PauseButtonW = 100;

        /// <summary>
        /// Init destinaion save path.
        /// </summary>
        public static string SavePath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, @"Resource\highscore.txt");

        /// <summary>
        /// init soundPaht.
        /// </summary>
        private static string bulletSoundPath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, @"Resource\gunFire_sound.wav");

        /// <summary>
        /// init gameStartSoundPath.
        /// </summary>
        private static string gameStartSoundPath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, @"Resource\gameStart_sound.wav");

        /// <summary>
        /// init bulletFire sounds.
        /// </summary>
        public static System.Media.SoundPlayer BulletFireSound = new System.Media.SoundPlayer(Config.bulletSoundPath);

        /// <summary>
        /// Init gameSound.
        /// </summary>
        public static System.Media.SoundPlayer GameStartSound = new System.Media.SoundPlayer(Config.gameStartSoundPath);
    }
}
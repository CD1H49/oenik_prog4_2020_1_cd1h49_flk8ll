﻿// <copyright file="IGameModel.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// IGameModel interface. Contains the model objects.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets Score.
        /// </summary>
        int Score { get; set; }

        /// <summary>
        /// Gets or sets lifes.
        /// </summary>
        int Lifes { get; set; }

        /// <summary>
        /// Gets or sets Ghosts.
        /// </summary>
        List<Ghost> Ghosts { get; set; }

        /// <summary>
        /// Gets or sets Bullets.
        /// </summary>
        List<Bullet> Bullets { get; set; }

        /// <summary>
        /// Gets or sets Pilot.
        /// </summary>
        Pilot Pilot { get; set; }

        /// <summary>
        /// Gets or sets FireRate.
        /// </summary>
        double FireRate { get; set; }

        /// <summary>
        /// Gets or sets HighScore.
        /// </summary>
        int HighScore { get; set; }

        /// <summary>
        /// Gets or sets CurrentLevel.
        /// </summary>
        double CurrentLevel { get; set; }

        /// <summary>
        /// Gets or sets TempFirerate.
        /// </summary>
        double TempFireRate { get; set; }

        /// <summary>
        /// Gets or sets GhostsStance.
        /// </summary>
        double GhostStance { get; set; }

        /// <summary>
        /// Gets or sets PilotStance.
        /// </summary>
        double PilotStance { get; set; }

        /// <summary>
        /// Gets or sets BulletStance.
        /// </summary>
        double BulletStance { get; set; }

        /// <summary>
        /// Gets or sets Background.
        /// </summary>
        Point[] Background { get; set; }
    }
}

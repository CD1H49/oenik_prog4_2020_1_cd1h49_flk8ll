﻿// <copyright file="GameModel.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Implementing GameModel Class.
    /// </summary>
    public class GameModel : IGameModel
    {
        /// <summary>
        /// Gets or sets pilot instance.
        /// </summary>
        public Pilot Pilot { get; set; }

        /// <summary>
        /// Gets or sets pilot instance.
        /// </summary>
        public List<Ghost> Ghosts { get; set; }

        /// <summary>
        /// Gets or sets Ghost list instance.
        /// </summary>
        public List<Bullet> Bullets { get; set; }

        /// <summary>
        /// Gets or sets score instance.
        /// </summary>
        public int Score { get; set; }

        /// <summary>
        /// Gets or sets lifes instance.
        /// </summary>
        public int Lifes { get; set; }

        /// <summary>
        /// Gets or sets HighScore instance.
        /// </summary>
        public int HighScore { get; set; }

        /// <summary>
        /// Gets or sets CurrentLevel instance.
        /// </summary>
        public double CurrentLevel { get; set; }

        /// <summary>
        /// Gets or sets FireRate instance.
        /// </summary>
        public double FireRate { get; set; }

        /// <summary>
        /// Gets or sets TempFireRate instance.
        /// </summary>
        public double TempFireRate { get; set; }

        /// <summary>
        /// Gets or sets GhostStance instance.
        /// </summary>
        public double GhostStance { get; set; }

        /// <summary>
        /// Gets or sets PilotStance instance.
        /// </summary>
        public double PilotStance { get; set; }

        /// <summary>
        /// Gets or sets BulletStance instance.
        /// </summary>
        public double BulletStance { get; set; }

        /// <summary>
        /// Gets or sets Background array instance.
        /// </summary>
        public Point[] Background { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
#pragma warning disable SA1201 // Elements should appear in the correct order
        public GameModel()
#pragma warning restore SA1201 // Elements should appear in the correct order
        {
            this.Ghosts = new List<Ghost>();
            this.Pilot = new Pilot();
            this.Bullets = new List<Bullet>();

            this.Ghosts.Add(new Ghost(Config.ResolutionWidth, Config.ResolutionHeight / 2));
            this.Ghosts.Add(new Ghost(Config.ResolutionWidth + 300, Config.ResolutionHeight / 4));
            this.Ghosts.Add(new Ghost(Config.ResolutionWidth + 700, 200));

            this.Background = new Point[2];
            this.Background[0] = new Point(0, 0);
            this.Background[1] = new Point(Config.ResolutionWidth - 1, 0);

            this.Score = 0;
            this.Lifes = 3;
            this.CurrentLevel = 1;
            this.FireRate = 5;
            this.GhostStance = 1;
            this.PilotStance = 1;
        }
    }
}

﻿// <copyright file="Ghost.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Implementing Ghost class.
    /// </summary>
    public class Ghost : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Ghost"/> class.
        /// </summary>
        /// <param name="x">X parameter.</param>
        /// <param name="y">Y parameter.</param>
        public Ghost(double x, double y)
        {
            this.area = new Rect(x, y, Config.GhostWidth, Config.GhostHeight);
        }
    }
}

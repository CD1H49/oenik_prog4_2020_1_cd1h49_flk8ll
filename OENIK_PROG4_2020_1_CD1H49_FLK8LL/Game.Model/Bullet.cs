﻿// <copyright file="Bullet.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Implementing Bullet class.
    /// </summary>
    public class Bullet : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bullet"/> class.
        /// </summary>
        /// <param name="x">X param.</param>
        /// <param name="y">Y param.</param>
        public Bullet(double x, double y)
        {
            this.area = new Rect(x, y, Config.BulletWidth, Config.BulletHeight);
            this.IsHit = false;
            this.BulletStance = 1;
        }

        /// <summary>
        /// Gets or sets a value indicating whether gest isHit variable.
        /// </summary>
        public bool IsHit { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gest IsDead variable.
        /// </summary>
        public bool IsDead { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gest BulletStance variable.
        /// </summary>
        public double BulletStance { get; set; }
    }
}

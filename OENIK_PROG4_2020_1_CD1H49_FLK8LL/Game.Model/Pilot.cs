﻿// <copyright file="Pilot.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Init Pilot class.
    /// </summary>
    public class Pilot : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pilot"/> class.
        /// </summary>
        public Pilot()
        {
            this.area = new Rect(0, Config.ResolutionHeight / 2, Config.PilotWidth, Config.PilotHeight);
        }
    }
}

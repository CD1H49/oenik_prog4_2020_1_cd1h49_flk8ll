﻿// <copyright file="GameItem.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Implementing GameItem abstract class.
    /// </summary>
    public abstract class GameItem
    {/// <summary>
    /// Create a Rect.
    /// </summary>
#pragma warning disable SA1401 // Fields should be private
        protected Rect area;
#pragma warning restore SA1401 // Fields should be private
        /// <summary>
        /// Gets  Area.
        /// </summary>
        public Rect Area
        {
            get { return this.area; }
        }

        /// <summary>
        /// Gets  x and y coordinates.
        /// </summary>
        public Point GetPoint
        {
            get { return new Point(this.area.X, this.area.Y); }
        }

        /// <summary>
        /// Set X,Y with given params.
        /// </summary>
        /// <param name="x">X coordinate.</param>
        /// <param name="y">Y coordinate.</param>
        public void SetXY(double x, double y)
        {
            this.area.X = x;
            this.area.Y = y;
        }

        /// <summary>
        /// Changing X coordinate with given param.
        /// </summary>
        /// <param name="x">Changed X coordinate.</param>
        public void ChangePointX(double x)
        {
            this.area.X += x;
        }

        /// <summary>
        /// Changing Y coordinate with given param.
        /// </summary>
        /// <param name="y">Changed Y coordinate.</param>
        public void ChangePointY(double y)
        {
            this.area.Y += y;
        }
    }
}

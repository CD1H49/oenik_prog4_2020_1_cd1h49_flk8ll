﻿// <copyright file="LogicTest.cs" company="CD1H49-FLK8LL">
// Copyright (c) CD1H49-FLK8LL. All rights reserved.
// </copyright>

namespace Game.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using Game.Logic;
    using Game.Model;
    using NUnit.Framework;
    using NUnit.Framework.Interfaces;

    /// <summary>
    /// Create The logic Test class.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private GameLogic logic;
        private GameModel model;

        /// <summary>
        /// set up the gamelogic and the gamemodel class,bullets.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.model = new GameModel();
            this.logic = new GameLogic(this.model);
            this.model.Bullets.Add(new Bullet(10, 10));
            this.model.Bullets.Add(new Bullet(5, 8));
            this.model.Bullets.Add(new Bullet(10, 20));
            this.model.Bullets.Add(new Bullet(1270, 1000));
        }

        /// <summary>
        /// Testing the MoveBackGround method.
        /// </summary>
        [Test]
        public void MoveBackgroundTest()
        {
            int step = 2;
            for (int i = 0; i < step; i++)
            {
                this.logic.MoveBackGroundOneStep(i);
            }

            Assert.AreEqual(1259.0d, this.model.Background[step - 1].X);
        }

        /// <summary>
        /// Testing moveGhost method.
        /// </summary>
        [Test]
        public void MoveGhostTest()
        {
            double resWidth = 1000;
            double resHigh = 500;
            double speed = -5;

            foreach (var ghost in this.model.Ghosts)
            {
                ghost.SetXY(resWidth, resHigh);
                ghost.ChangePointX(speed);
                Assert.AreEqual(995, ghost.GetPoint.X);
            }
        }

        /// <summary>
        /// Testing MoveBullet method.
        /// </summary>
        [Test]
        public void MoveBulletsTest()
        {
            double speed = -5;
            Bullet b = this.model.Bullets.First();
            b.ChangePointX(speed);
            Assert.AreEqual(5, b.GetPoint.X);
        }

        /// <summary>
        /// Testing MouseControll.
        /// </summary>
        [Test]
        public void MouseControlTest()
        {
            double x = 10;
            double y = 10;
            Point p = new Point
            {
                X = x,
                Y = y,
            };
            this.model.Pilot.SetXY(x, y);

            Assert.AreEqual(p, this.model.Pilot.GetPoint);
        }

        /// <summary>
        /// Testing adding bullet to the bullet list.
        /// </summary>
        [Test]
        public void AddBulletTest()
        {
            double x = 10;
            double y = 10;
            this.model.Bullets.Add(new Bullet(x, y));
            Assert.IsNotNull(this.model.Bullets);
            Assert.AreEqual(5, this.model.Bullets.Count);
        }

        /// <summary>
        /// Testing ChangeFireRates.
        /// </summary>
        [Test]
        public void ChangeFireRateTest()
        {
            int fireRate = 10;
            int addFire = 1;
            this.model.FireRate = fireRate;
            fireRate += addFire;

            Assert.AreEqual(11, fireRate);
        }

        /// <summary>
        /// Testing Deleting Bullet item.
        /// </summary>
        [Test]
        public void DeleteBulletTest()
        {
            double resWidth = 1260;
            int bulletCount = 3;
            foreach (var bullet in this.model.Bullets.ToList())
            {
                if (bullet.GetPoint.X > resWidth)
                {
                    this.logic.DeleteBullet();
                }
            }

            Assert.AreEqual(bulletCount, this.model.Bullets.Count);
        }
    }
}
